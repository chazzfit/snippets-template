Docker
######

.. contents::
  :backlinks: top


.. figure::  ../_static/docker/docker_logo.png
  :align: center
  :width: 300


Commandes simples
*****************

Create Image
------------
.. code-block:: bash

    docker build -t <TAG_NAME> .